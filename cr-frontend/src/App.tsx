import React, { useEffect, useState } from 'react';
import { Course } from './Interfaces'
import './App.css';
import CourseItem from './CourseItem'

const App = () => { //component
  const [courses, setCourses] = useState<Course[]>([]); // Hook useState

  useEffect(() => { // Hook useEffect
    fetch('http://localhost:3000/courses/')
      .then(res => res.json())
      .then(courses => {
        // console.log(courses);
        setCourses(courses);
    })
  },[]); //ใส่ [] เพื่อไม่ให้ useEffect เรียกหลายรอบ

  return (
    <div className="App">
      <ul>
        {courses.map(item => (
          <CourseItem key={item.id} course={item} />
        ))}
      </ul>
    </div>
  )
}

// type AppState = {
//   message: string;
// };

// class App extends React.Component<{}, AppState> {
//   state: AppState = {
//     message: 'Default Message',
//   };

//   componentDidMount() {
//     fetch('http://localhost:3000/courses/')
//       .then(res => res.json())
//       .then(obj => {
//         this.setState({message: obj.message});
//     })
//   }
  
//   render() {
//     return (
//       <div>
//        {this.state.message}
//       </div>
//     );
//   }
// }

export default App;
