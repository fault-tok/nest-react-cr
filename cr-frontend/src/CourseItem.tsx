import React from 'react';
import { Course } from "./Interfaces";

type CourseItemProp = {
    course: Course;
}

//ฟังก์ชัน Component
const CourseItem = (props: CourseItemProp) => {
    const course: Course = props.course;
    
    return (
        <li className='Course'>{course.number} - {course.title}</li>
    );
};

export default CourseItem;