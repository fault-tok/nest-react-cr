// import { Course } from './interface/couses.interface';
import { Injectable } from "@nestjs/common";
import { Course } from "./interface/couse.interface";


@Injectable()
export class CoursesService{
    async findAll(): Promise<Course[]> {
        return [
          {
            id: '001',
            number: '100001',
            title: 'Java'
          },
          {
            id: '002',
            number: '100002',
            title: 'JavaScript'
          },
          {
            id: '003',
            number: '100003',
            title: 'Python'
          }
        ];
      }
}